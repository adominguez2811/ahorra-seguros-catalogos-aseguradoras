USE [GNPCATALOGOS]
GO

/****** Object:  Table [dbo].[GNP_CATALOGOS]    Script Date: 26/07/2019 11:27:30 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GNP_CATALOGOS](
	[TIPO_VEHICULO] [nvarchar](max) NOT NULL,
	[ARMADORA] [nvarchar](max) NOT NULL,
	[MODELO] [nvarchar](max) NOT NULL,
	[CARROCERIA] [nvarchar](max) NOT NULL,
	[VERSION_T] [nvarchar](max) NOT NULL,
	[CLAVE_MARCA] [nvarchar](max) NOT NULL,
	[ALTOVALOR] [nvarchar](max) NOT NULL,
	[CLAVE] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

