USE [GNPCATALOGOS]
GO

/****** Object:  Table [dbo].[GNP_MODELO]    Script Date: 26/07/2019 11:27:37 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GNP_MODELO](
	[CLAVE] [nvarchar](6) NOT NULL,
	[VALOR] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

