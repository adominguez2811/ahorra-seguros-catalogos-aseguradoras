USE [GNPCATALOGOS]
GO

/****** Object:  Table [dbo].[GNP_VERSION_VEHICULO]    Script Date: 26/07/2019 11:28:25 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GNP_VERSION_VEHICULO](
	[CLAVE] [nvarchar](5) NOT NULL,
	[NOMBRE] [nvarchar](max) NOT NULL,
	[VALOR] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

