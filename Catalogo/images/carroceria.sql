USE [GNPCATALOGOS]
GO

/****** Object:  Table [dbo].[GNP_CARROCERIA]    Script Date: 26/07/2019 11:27:15 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GNP_CARROCERIA](
	[CLAVE] [nvarchar](5) NOT NULL,
	[NOMBRE] [nvarchar](max) NOT NULL,
	[VALOR] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

