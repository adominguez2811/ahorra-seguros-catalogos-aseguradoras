USE [GNPCATALOGOS]
GO

/****** Object:  Table [dbo].[GNP_TIPO_CARGA]    Script Date: 26/07/2019 11:27:51 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GNP_TIPO_CARGA](
	[CLAVE] [nvarchar](5) NOT NULL,
	[NOMBRE] [nvarchar](max) NOT NULL,
	[VALOR] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

