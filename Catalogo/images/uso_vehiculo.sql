USE [GNPCATALOGOS]
GO

/****** Object:  Table [dbo].[GNP_USO_VEHICULO]    Script Date: 26/07/2019 11:28:14 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GNP_USO_VEHICULO](
	[CLAVE] [nvarchar](5) NOT NULL,
	[NOMBRE] [nvarchar](max) NOT NULL,
	[VALOR] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

