﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Aseguradoras_GNP : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session("Administrador") == "")
        //{
        //    Response.Redirect("/Login.aspx");
        //}

    }

    protected void btnCargaGnP_Click(object sender, EventArgs e)
    {
        String[] xmls = { "VEHICULOS", "ARMADORA_VEHICULO", "CARROCERIA_VEHICULO", "MODELO_VEHICULO", "TIPO_CARGA_VEHICULO", "TIPO_VEHICULO", "USO_VEHICULO", "VERSION_VEHICULO" };
        String[] xmlDesc = { "VehiculosGeneral", "Armadoras", "Carroceria", "Modelo", "TipoCarga", "TipoVehiculo", "UsoVehiculo", "VersionVehiculo" };
        String responseLoc = "<br>";
        for (int i = 0; i < 8; i++)
        {
            String mixml = "<SOLICITUD_CATALOGO><USUARIO>IPEREZ050031</USUARIO><PASSWORD>Junipg2017</PASSWORD><TIPO_CATALOGO>" + xmls[i] + "</TIPO_CATALOGO><ID_UNIDAD_OPERABLE/><FECHA>01/01/1800</FECHA></SOLICITUD_CATALOGO>";
            responseLoc += this.ApiConsume("https://api.service.gnp.com.mx/autos/wsp/catalogos/catalogo", mixml, xmlDesc[i]);
        }

        //se muestran las respuestas de la operacion en un label
       
        Label1.Text = responseLoc;
        Label2.Text = StopProcedure();
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Carga completa')", true);
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Carga completa')", true);
        //Response.Redirect("../Aseguradoras/GNP.aspx");
        //Response.Write("<script>alert('Carga completa');</script>");
        cargaExito();


    }
    public String ApiConsume(String url, String Content, String version)
    {
        //Creacion del objeto RestSharp
        var client = new RestClient(url);
        //Añadiendo las credenciales para la conexion para el objeto
        client.Authenticator = new HttpBasicAuthenticator("IPEREZ050031", "Junipg2017");
        //Creacion de un request por metodo post
        var request = new RestRequest(Method.POST);
        //Declaracion de los headers del objeto declaracion 
        //Le decimos que acepte respuestas de todo tipo sin importar el formato 
        request.AddHeader("Accept", "*/*");
        //Declaramos que el tipo de cuerpo va a ser en xml
        request.AddHeader("Content-Type", "application/xml;");
        //Añadimos el cuerpo a el request
        request.AddParameter("application/xml", Content, ParameterType.RequestBody);
        //Ejecutamos el request y cuardamos la respuesta en un objeto de tipo IRestResponse que es parte de RestSharp
        IRestResponse response = client.Execute(request);
        if (response.StatusCode != System.Net.HttpStatusCode.OK)
        {
            return response.Content + "error en la peticion de " + version + "<br>";
        }
        //Guardamos el resultado obtenido en un archivo de xml 
        try
        {
            FileStream file = File.Create(@"C:\Users\ultron\Desktop\" + version + ".xml");
            Byte[] info = new UTF8Encoding(true).GetBytes(response.Content);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception exc)
        {
            return exc.Message + "Error al generar el archivo " + version + "<br>";
        }

        return version + " ok<br>";
    }
    public string StopProcedure()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=GNPCATALOGOS;Integrated Security=True"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("Sp_GNP_CATALOGO", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();
                    return command.ToString();

                }
            }
        }
        catch (Exception excep)
        {
            return excep.Message;
        }


    }

    public void cargaExito()
    {
        
        Response.Write("<script>alert('Carga completa');</script>");
    }

}