﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel.Security;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Aseguradoras_ElAguila : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public List<Marca> ObtenerSubMarcas(List<Marca> lista, ElAguila.CatCotizadorClient aguila, String pass)
    {
        List<Marca> submarca = new List<Marca>();
        String salida = "";
        foreach (Marca marc in lista)
        {
            String[] cadenas = marc.anios.Split(',');
            foreach (String str in cadenas)
            {
                int clave = int.Parse(str);
                ElAguila.TypCatalogString[] Agstr = aguila.SubMarca(pass, clave, int.Parse(marc.clv), "R", marc.desc);
                foreach (ElAguila.TypCatalogString strA in Agstr)
                {
                    Marca marcas = new Marca(strA.strCve, strA.strDesc, str, int.Parse(marc.clv));
                    submarca.Add(marcas);
                    salida += marcas.clv + "|" + marcas.desc + "|" + marcas.anios + "|" + marc.clv + "<br>";
                }
            }
        }
        CrearTxt(salida, "submarca");
        return submarca;

    }

    public String find(List<Marca> lista, int marca)
    {
        foreach (Marca marc in lista)
        {
            if (int.Parse(marc.clv) == marca)
                return marc.desc;
        }

        return "false";
    }

    public Boolean ObtenerSubMarcasTipo(List<Marca> lista, List<Marca> Sublista, ElAguila.CatCotizadorClient aguila, String pass)
    {
        String salida = "";
        int i = 0;
        int clvMarca = -1;
        String nombre = "";
        foreach (Marca marcas in Sublista)
        {
            if (clvMarca != marcas.clv_marca)
                nombre = find(lista, marcas.clv_marca);

            ElAguila.TypCatalogString[] astr = aguila.SubMarcaTipo(pass, int.Parse(marcas.anios), "R", marcas.clv_marca, nombre, int.Parse(marcas.clv), "?");
            foreach (ElAguila.TypCatalogString agstr in astr)
            {
                salida += agstr.strCve + "|" + agstr.strDesc + "|" + marcas.anios + "|" + marcas.clv_marca + "|" + marcas.clv + "<br>";
                System.Diagnostics.Debug.WriteLine("SubMarcasTipo: " + marcas.desc + nombre + marcas.anios + "");
                i++;
            }
            System.Diagnostics.Debug.WriteLine(marcas.desc + " tenia: " + i.ToString() + " campos");
            i = 0;
        }



        CrearTxt(salida, "SubMarcaTipo");

        return true;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

        ElAguila.CatCotizadorClient aguila = new ElAguila.CatCotizadorClient();
        String password = "AgtTrigarante2017#%";
        String SalidaCreacion = "";
        String SalidaAlmacenaje = "";
        List<Marca> listaMarca = ObtenerMarcas(password, aguila);
        String[] names = { "Sp_ELAGUILA_CATALOGO", "Sp_ELAGUILA_SUBMARCA", "Sp_ELAGUILA_SUBMARCA_TIPO" };

        if (listaMarca.Count != 0)
        {
            List<Marca> listaSubMarca = ObtenerSubMarcas(listaMarca, aguila, password);
            if (listaSubMarca.Count != 0)
            {
                SalidaCreacion += "Se crearon los archivos correctamente";


                if (ObtenerSubMarcasTipo(listaMarca, listaSubMarca, aguila, password))
                {
                    SalidaCreacion += "Se crearon los archivos correctamente";
                }
                else
                {
                    SalidaCreacion += "ah habido un error en la creacion de los archivos";
                }
            }
        }
        else
        {
            SalidaCreacion += "hubo un error al generar los archivos";
        }

        foreach (String name in names)
        {
            if (Almacenar(name) == "ok")
                SalidaAlmacenaje += "Se almacenaron los archivos correctamente";
            else
                SalidaAlmacenaje += "hubo un error al momento de almacenar";
        }

        Label1.Text = SalidaCreacion;
        Label2.Text = SalidaAlmacenaje;
        //---------------------------------------AXXA-------------------------------------------------
        //AXAWS.integratorPortClient AXAS = new AXAWS.integratorPortClient();
        //AXAWS.catalogueRequest resq = new AXAWS.catalogueRequest();
        //AXAWS.agentInfo agent = new AXAWS.agentInfo();
        //agent.agentCode = "AGT00607034";
        //agent.cessionPercentage = 0;
        //agent.participationPercentage = "0";
        //resq.agentInfo = agent;

        //AXAWS.catalogue cat = new AXAWS.catalogue();
        //cat.catalogueName = "Estado Civil";
        //resq.catalogue = cat;

        //AXAWS.catalogueResponse res = AXAS.getCatalogueDetails(resq);

        //Label1.Text = res.catalogueDetails.description.ToString();

        //---------------------------------------Mapfre-------------------------------------------------

        //MapfreWS.CatalogosAutosSoapClient service = new MapfreWS.CatalogosAutosSoapClient();
        //Label1.Text = service.WS_TW_Marcas("<![CDATA[<xml><data><valor cod_ramo='45' cod_zona_agt='99' cod_tip_vehi='5' anio_fabrica='2010' id_negocio='LOCALES'/></data></xml>]]>", "20190724").OuterXml.ToString();

        //QualitasSoap.wsTarifaSoapClient soeap = new QualitasSoap.wsTarifaSoapClient();
        //String result = soeap.listaTarifas("AhorraSeguros", "AhorraS3guros2017", "","","","","","","").ToString();
        //Label1.Text = result;
    }


    public List<Marca> ObtenerMarcas(String password, ElAguila.CatCotizadorClient aguila)
    {

        String salida = "";
        List<Marca> listS = new List<Marca>();
        int flag = 0;
        for (int i = 1981; i < DateTime.Now.Year + 2; i++)
        {
            ElAguila.TypCatalogString[] Agstr = aguila.Marca(password, i, "R");
            foreach (ElAguila.TypCatalogString str in Agstr)
            {
                if (listS.Count() == 0)
                {
                    Marca marca = new Marca(str.strCve, str.strDesc, i.ToString(), 0);
                    listS.Add(marca);
                }
                else
                {
                    foreach (Marca marc in listS)
                    {
                        if (marc.clv == str.strCve)
                        {
                            marc.ModificarAnio(i.ToString());
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 0)
                    {
                        Marca marca = new Marca(str.strCve, str.strDesc, i.ToString(), 0);
                        listS.Add(marca);
                    }
                    else
                        flag = 0;
                }
            }
        }

        foreach (Marca marca in listS)
        {
            salida += marca.clv + "|" + marca.desc + "|" + marca.anios + "<br>";
        }

        CrearTxt(salida, "MarcaElAguila");

        return listS;
    }

    public void CrearTxt(String salida, String name)
    {
        try
        {
            FileStream file = File.Create(@"C:\Users\ultron\Desktop\" + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception exc)
        {

        }
    }

    public String Almacenar(String store)
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=ElAguilaCatalogos;Integrated Security=True"))
              
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand(store, conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return "ok";
                }
            }
        }
        catch (Exception excep)
        {
            return excep.Message;
        }
    }

}

public class Marca
{

    public String clv { get; set; }
    public String desc { get; set; }
    public String anios { get; set; }
    public int clv_marca { get; set; }

    public Marca(String clave, String desc, String anios, int clv_marca)
    {
        this.clv = clave;
        this.desc = desc;
        this.anios = anios;
        this.clv_marca = clv_marca;
    }

    public void ModificarAnio(String anio)
    {
        this.anios += "," + anio;
    }

}