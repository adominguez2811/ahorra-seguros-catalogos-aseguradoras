﻿
Partial Class Principal
    Inherits System.Web.UI.MasterPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Administrador") = "" Then
            Response.Redirect("/Login.aspx")
        End If
        LblMostrar.Text = "Bienvenido " + Session("Administrador")

    End Sub

End Class

