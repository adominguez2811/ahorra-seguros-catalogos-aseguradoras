﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Descripción breve de BD
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
// [System.Web.Script.Services.ScriptService]
public class BD : System.Web.Services.WebService
{

    public BD()
    {

        //Elimine la marca de comentario de la línea siguiente si utiliza los componentes diseñados 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hola a todos";
    }

    [WebMethod]
    public DataSet GnpCataloos()
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = @"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=ws_seguros_auto;Integrated Security=True";
        SqlDataAdapter da = new SqlDataAdapter("select * from [dbo].[CatAseguradoras]", conn);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    //[WebMethod]
    //public  obtenerCatalogoGNP ()
    //{

    //}


}
